package handlers

import (
	"encoding/json"
	"fmt"
	"net/http"

	"gitlab.com/upchieve/two-am-takeout/server/sockets"
)

// HealthBody is the response body for a health check
//HealthBody struct was incorrect; expecting a lowercase ok in the JSON object
//Structs are similar to interfaces
type HealthBody struct {
	OK bool `json:"ok"`
}

// ServeHealth handles healthcheck requests
func ServeHealth(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Access-Control-Allow-Origin", "http://localhost:8080")
	//Send a true value with the ok value as JSON
	// the Healthbody is expecting a boolean which we feed as "true"

	json.NewEncoder(w).Encode(HealthBody{OK: true})
}

// ServeSocket returns a socket handler.
// This handler is only checking for connections NOT for messages sent
func ServeSocket(h *sockets.Hub) func(w http.ResponseWriter, r *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		fmt.Println("HIT FROM SERVE SOCKET!")
		sockets.ServeWs(h, w, r)
	}
}
