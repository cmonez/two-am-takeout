import Vue from 'vue';
import Vuetify from 'vuetify/lib/framework';
// import colors from 'vuetify/lib/util/colors';

Vue.use(Vuetify);

export default new Vuetify({
  theme: {
    themes: {
      light: {
        primary: '#16d2aa',
        secondary: 'white',
        error: 'white',
      },
      dark: {
        primary: '52057b',
        secondary: '#404040',
        error: '#001f3f',
        success: 'white',
      },
    },
  },
});
