# main.go File

## Packages

- Immediately notice we import packages similarly to node
- Also I like how each package has what looks like JS methods? the package itself is lowercased but the methods are uppercased!

## main func

- main func calls the function defined below it
- seems like files run from a "main" func in go

## Server

- Server creation exists in a giant function
- Looks like its assigned to a variable using ":=" notation "srv"
- Setting up custom port
- Takes router variable

```
			srv := &http.Server{
				Handler:      r,
				Addr:         "127.0.0.1:3000",
				WriteTimeout: 15 * time.Second,
				ReadTimeout:  15 * time.Second,
			}
```

## Router

- Router created with a function call from package
- HandleFunc looks at url paths
- handlers.ServeHealth --> works like middleware, handlers.ServerHealth method interacts with GETs @ health path

```

    r.HandleFunc("/health", handlers.ServeHealth).Methods("Get")

```

- reminds me of:

```
    app.get('/', function (req, res) {
    })
```

- Looks like global handler on all routes. Makes sense for CORS

```
  r.Use(mux.CORSMethodMiddleware(r))
```

## Context

- Referenced in main func and startServer where it's a parameter
  [link](https://gobyexample.com/context)

## Goroutines

- [Context on goroutines](https://gobyexample.com/goroutines)
- Looks like an aysnc way to run code
- [More infromation on concurrency in go](https://dev.to/nadirbasalamah/golang-tutorial-9-concurrency-using-goroutine-2mp6)

## Test Suite

- Creating mock server on the same port as our regular server (can't run the test and suite simulataneously)

## Test 1

```
type HealthBody struct {
	OK bool `json:"OK"`
}

// ServeHealth handles healthcheck requests
func ServeHealth(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Access-Control-Allow-Origin", "http://localhost:8080")
	//Send a true value with the ok value as JSON
	// the Healthbody is expecting a boolean which we feed as "true"

	json.NewEncoder(w).Encode(HealthBody{OK: true})
}

```

![Failing Test](failingTest.png)

- Whats going on? The struct interface is being fed a capitalized "OK"
- Within the test suite, we are expecting a LOWERCASED "ok"

```
    checkBackend: function() {
      axios
        .get('http://localhost:3000/health')
        .then((res) => {
          if (res.status === 200 && res.data.ok === true) {
            console.log('backend is healthy');
            this.backendHealthy = true;
            return;
          } else {
```

- On the front end, we call the method checkBackend when the instance is [created](https://vuejs.org/v2/api/#created)
- Looking at our response from the backend, we are expecting a data object with a lowercased "ok"
- Given that the struct is uppercased once (vs twice for lowercase), we can just change the structure of the struct

```
type HealthBody struct {
	OK bool `json:"ok"`
}
```

## Hub

- [Hubs](https://github.com/gorilla/websocket/tree/master/examples/chat)
- Updgrader takes your connection and upgrades it
- Whats happening below
- Takes in response writer, w, request, r, and nil

## Client

- The actors that communicate with the hub
- Seems like the hub is first created and then the client interacts with hub through three given actions (register, unregiste, message)
- [Upgarder](https://stackoverflow.com/questions/50204967/what-is-websocket-upgrader-exactly) grabs a connection and reconverts it

```
func ServeWs(hub *Hub, w http.ResponseWriter, r *http.Request) {
	conn, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		log.Println(err)
		return
	}
	client := &Client{hub: hub, conn: conn, send: make(chan []byte, 256)}
	client.hub.register <- client
	// Allow collection of memory referenced by the caller by doing all work in
	// new goroutines.
	go client.writePump()
	go client.readPump()
}
```

## Test 2

- The suite is testing if messages can be received through websockets
- We know from our front end that the first point of communication with websockets on the backend is upon hitting the site
- The SOCKET_ONOPEN function describes this expectation
- Within the hub.go file I am logging messages based on the client interactions with backend socket

```
func (h *Hub) Run() {
	for {
		select {
		case client := <-h.register:
			h.clients[client] = true
			fmt.Println("Client has joined!")
		case client := <-h.unregister:
			if _, ok := h.clients[client]; ok {
				delete(h.clients, client)
				close(client.send)
				fmt.Println("Client is leaving!")
			}
		case message := <-h.broadcast:
			fmt.Println("Message")
			for client := range h.clients {
				select {
				case client.send <- message:
				default:
					close(client.send)
					delete(h.clients, client)
```

- Below is what gets logged upon reaching the site
- ![Failing Test](messages.png)
- I am logging from within the ServeSocket handler and the hub
- Once I send messages back and forth NEITHER of these messages are logged, but the we see a "Message" log
- Like we said above, the first point of communcation with the socket is upon connection
- What does this mean? Perhaps instead of testing whether messages can be sent, we should be testing if connections can be made

## Chat

- LOVING v-model [input bindings](https://vuejs.org/v2/guide/forms.html) in vue!!
- Usually in React i'd create generalized [onChange handler](https://www.pluralsight.com/guides/handling-multiple-inputs-with-single-onchange-handler-react) which can be kinda verbose.
- Vue's approach looks a lot simpler and more intuitive since the binding is seemingly baked into the HTML

```
    v-model="userName"
```

- Have to pay attention to color of the chat bubbles
- Chat bubbles in green are coming from you
- Chat bubbles in white are coming from other users
- Add nane data only to chat bubbles in white
- How do we know to color other peoples messages?
- Created function runs on page load; notice that a uuid is also created
- This is the uuid used to identify you
- [Created](https://vuejs.org/v2/api/#created) As per the docs "Called synchronously after the instance is created"
- Sounds similar to the componentDidMount method in React which is invoked right after a component is mounted
- [Vue Life Cycle](https://vuejs.org/v2/guide/instance.html#Lifecycle-Diagram)

```
  v-if="message.uuid === uuid"
```

- As we iterate for each message in the data, we check to see if the [uuid](https://en.wikipedia.org/wiki/Universally_unique_identifier) in the message matches the uuid in our data (similar to state in React)
- If match, color the v-chip green
- Otherwise color it white
- Love the conditional rendering in vue as well! Same observation as with the v-model
- Add a dash and name to messages from other users
- Similar to rendering props in React, using curly bracket syntax
- ![](nameexample.gif)
- If I had more time, I would add functionality to broadcast name changes. This would involve keeping a reference to the the old name variable and showing a message within the chatbox (meaning we'd have to interact with the backend). Also would mean i'd potentially need to have a button that will only register name saves upon click. That way, the call to the backend would not occur upon changing the text each time and our chat box wouldn't be flooded with announcements each time a user types into the user field

## Vue themes

- As per the [vuetify docs](https://vuetifyjs.com/en/features/theme/#theme-generator) it looks like you can cuztomize your applications very easily!
- When instantiating your vuetify app you can simply give it "light" and "dark" themes
- Within your components, you just reference the variable name
- What am i doing here?

```
  <v-btn icon v-on:click="toggleDark">
        <v-icon>mdi-theme-light-dark</v-icon>
  </v-btn>

  methods: {
    toggleDark: function() {
      this.$vuetify.theme.dark = !this.$vuetify.theme.dark;
    },
  },
```

- This method is tied to the instantiated vue app
-

```
export default new Vuetify({
  theme: {
    themes: {
      light: {
        primary: '#16d2aa',
        secondary: 'white',
        error: 'white',
      },
      dark: {
        primary: '52057b',
        secondary: '#404040',
        error: '#001f3f',
        success: 'white',
      },
    },
  },
});
```

- As per the docs "You can manually turn dark on and off by changing this.\$vuetify.theme.dark to true or false."
- What the toggleDark method is jumping between light and dark theme
- ![](darkmode.gif)
